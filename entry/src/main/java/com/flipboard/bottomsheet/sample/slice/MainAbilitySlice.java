/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.flipboard.bottomsheet.sample.slice;


import com.flipboard.bottomsheet.BottomSheetLayout;
import com.flipboard.bottomsheet.sample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

public class MainAbilitySlice extends AbilitySlice {
    private Button intent_picker_button, image_picker_button,menu_picker_button,fragment_picker_button;
    protected Text titleView;
    private Button share_button;
    private Text share_text;
    protected BottomSheetLayout bottomSheetLayout;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        ComponentContainer rootLayout = (ComponentContainer) LayoutScatter.getInstance(this)
                .parse(ResourceTable.Layout_ability_main, null, false);

        intent_picker_button = (Button) rootLayout.findComponentById(ResourceTable.Id_picker_button);
        fragment_picker_button = (Button) rootLayout.findComponentById(ResourceTable.Id_bottomsheet_fragment_button);

        intent_picker_button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                present(new PickerAbilitySlice(), new Intent());

            }
        });
        fragment_picker_button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
               present(new AbilityTest(), new Intent());

            }
        });

        super.setUIContent(rootLayout);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
