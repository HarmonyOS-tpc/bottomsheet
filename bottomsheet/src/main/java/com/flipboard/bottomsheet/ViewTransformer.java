package com.flipboard.bottomsheet;

import ohos.agp.components.Component;

public interface ViewTransformer {

    void transformView(float translation, float maxTranslation, float peekedTranslation, BottomSheetLayout parent, Component view);
    float getDimAlpha(float translation, float maxTranslation, float peekedTranslation, BottomSheetLayout parent, Component view);
}
