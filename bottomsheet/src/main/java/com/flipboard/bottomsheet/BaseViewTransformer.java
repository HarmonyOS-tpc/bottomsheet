package com.flipboard.bottomsheet;

import ohos.agp.components.Component;

public abstract class BaseViewTransformer implements ViewTransformer {


    public static final float MAX_DIM_ALPHA = 0.7f;

    @Override
    public float getDimAlpha(float translation, float maxTranslation, float peekedTranslation, BottomSheetLayout parent, Component view) {
        float progress = translation / maxTranslation;
        return progress * MAX_DIM_ALPHA;
    }

}
