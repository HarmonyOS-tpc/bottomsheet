package com.flipboard.bottomsheet.commons;

import com.flipboard.bottomsheet.ViewTransformer;
import ohos.aafwk.ability.fraction.FractionManager;
import ohos.aafwk.ability.fraction.FractionScheduler;
import ohos.app.IAbilityManager;

public interface BottomSheetFragmentInterface {

    void show(FractionManager manager, int bottomSheetLayoutId);

    int show(FractionScheduler transaction,  int bottomSheetLayoutId);

    ViewTransformer getViewTransformer();
}
