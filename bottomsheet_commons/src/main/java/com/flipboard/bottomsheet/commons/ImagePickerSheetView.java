package com.flipboard.bottomsheet.commons;

import ohos.agp.components.Image;
import ohos.agp.components.StackLayout;
import ohos.app.Context;
import ohos.utils.net.Uri;

/**
 * A Sheet view for displaying recent images or options to pick or take pictures.
 */
public class ImagePickerSheetView extends StackLayout {

    /**
     * Callback for whenever a tile is selected in the sheet.
     */
    public interface OnTileSelectedListener {
        /**
         * @param selectedTile The selected tile, in the form of an {@link ImagePickerTile}
         */
        void onTileSelected(ImagePickerTile selectedTile);
    }

    /**
     * Interface for providing an image given the {@link }.
     */
    public interface ImageProvider {
        /**
         * This is called when the underlying adapter is ready to show an image
         *
         * @param imageView ImageView target. If you care about memory leaks and performance, DO NOT
         *                  HOLD ON TO THIS INSTANCE!
         * @param imageUri Uri for the image.
         * @param size Destination size of the image (it's a square, so assume this is the height
         *             and width).
         */
        void onProvideImage(Image imageView, Uri imageUri, int size);
    }

    /**
     * Backing class for image tiles in the grid.
     */
    public static class ImagePickerTile {

        public static final int IMAGE = 1;
        public static final int CAMERA = 2;
        public static final int PICKER = 3;

        public @interface TileType {}

        public @interface SpecialTileType {}

        protected final Uri imageUri;
        protected final @TileType int tileType;

        ImagePickerTile(@SpecialTileType int tileType) {
            this(null, tileType);
        }

        ImagePickerTile(Uri imageUri) {
            this(imageUri, IMAGE);
        }

        protected ImagePickerTile(Uri imageUri, @TileType int tileType) {
            this.imageUri = imageUri;
            this.tileType = tileType;
        }

        /**
         * @return The image Uri backing this tile. Can be null if this is a placeholder for the
         *         camera or picker tiles.
         */
        public Uri getImageUri() {
            return imageUri;
        }

        /**
         * @return The {@link TileType} of this tile: either {@link #IMAGE}, {@link #CAMERA}, or
         *         {@link #PICKER}
         */
        @TileType
        public int getTileType() {
            return tileType;
        }

        /**
         * Indicates whether or not this represents an image tile option. If it is, you can safely
         * retrieve the represented image's file Uri via {@link #getImageUri()}
         *
         * @return True if this is a camera option, false if not.
         */
        public boolean isImageTile() {
            return tileType == IMAGE;
        }

        /**
         * Indicates whether or not this represents the camera tile option. If it is, you should do
         * something to facilitate taking a picture, such as firing a camera intent or using your
         * own.
         *
         * @return True if this is a camera option, false if not.
         */
        public boolean isCameraTile() {
            return tileType == CAMERA;
        }

        /**
         * Indicates whether or not this represents the picker tile option. If it is, you should do
         * something to facilitate retrieving a picture from some other provider, such as firing an
         * image pick intent or retrieving it yourself.
         *
         * @return True if this is a picker tile, false if not.
         */
        public boolean isPickerTile() {
            return tileType == PICKER;
        }

        @Override
        public String toString() {
            if (isImageTile()) {
                return "ImageTile: " + imageUri;
            } else if (isCameraTile()) {
                return "CameraTile";
            } else if (isPickerTile()) {
                return "PickerTile";
            } else {
                return "Invalid item";
            }
        }
    }

    protected String title;
   /* private int columnWidthDp = 100;*/


    protected ImagePickerSheetView(final Builder builder) {
        super(builder.context);
        setTitle(builder.title);
    }

    public void setTitle(int titleRes) {
        setTitle("hello world");
    }

    public void setTitle(String title) {
    }
    public static class Builder {

        Context context;
        int maxItems = 25;
        String title = null;
        public Builder( Context context) {
            this.context = context;
        }

        /**
         * Sets the max number of tiles to show in the image picker. Default is 25 from local
         * storage and the two custom tiles.
         *
         * @param maxItems Max number of tiles to show
         * @return This builder instance
         */
      /*  public Builder setMaxItems(int maxItems) {
            this.maxItems = maxItems;
            return this;
        }*/

        /**
         * Sets a title via String resource ID.
         *
         * @param title String resource ID
         * @return This builder instance
         */
      /*  public Builder setTitle(@StringRes int title) {
            return setTitle(context.getString(title));
        }*/

        /**
         * Sets a title for this sheet. If the title param is null, then no title will be shown and
         * the title view will be hidden.
         *
         * @param title The title String
         * @return This builder instance
         */
        public Builder setTitle( String title) {
            this.title = title;
            return this;
        }

        /**
         * Sets a listener for when a tile is selected.
         *
         * @param onTileSelectedListener Listener instance
         * @return This builder instance
         */
      /*  public Builder setOnTileSelectedListener(OnTileSelectedListener onTileSelectedListener) {
            this.onTileSelectedListener = onTileSelectedListener;
            return this;
        }
*/
        /**
         * Sets a provider for providing images.
         *
         * @param imageProvider Provider instance
         * @return This builder instance
         */
     /*   public Builder setImageProvider(ImageProvider imageProvider) {
            this.imageProvider = imageProvider;
            return this;
        }*/

        /**
         * Sets a boolean to indicate whether or not to show a camera option.
         *
         * @param showCameraOption True to show the option, or false to hide the option
         * @return This builder instance
         */
     /*   public Builder setShowCameraOption(boolean showCameraOption) {
            this.showCameraOption = showCameraOption;
            return this;
        }*/

        /**
         * Sets a boolean to indicate whether or not to show the picker option.
         *
         * @param showPickerOption True to show the option, or false to hide the option.
         * @return This builder instance
         */
     /*   public Builder setShowPickerOption(boolean showPickerOption) {
            this.showPickerOption = showPickerOption;
            return this;
        }*/

        /**
         * Sets a drawable resource ID for the camera option tile. Default is to use the material
         * design version included in the library.
         *
         * @param resId Camera drawable resource ID
         * @return This builder instance
         */
      /*  public Builder setCameraDrawable(@DrawableRes int resId) {
            return setCameraDrawable(ResourcesCompat.getDrawable(context.getResources(), resId, null));
        }*/

        /**
         * Sets a drawable for the camera option tile. Default is to use the material design
         * version included in the library.
         *
         * @param cameraDrawable Camera drawable instance
         * @return This builder instance
         */
       /* public Builder setCameraDrawable(@Nullable Drawable cameraDrawable) {
            this.cameraDrawable = cameraDrawable;
            return this;
        }*/

        /**
         * Sets a drawable resource ID for the picker option tile. Default is to use the material
         * design version included in the library.
         *
         * @param resId Picker drawable resource ID
         * @return This builder instance
         */
      /*  public Builder setPickerDrawable(@DrawableRes int resId) {
            return setPickerDrawable(ResourcesCompat.getDrawable(context.getResources(), resId, null));
        }

        *//**
         * Sets a layout for the image tile which MUST have an ImageView as root view. Default is to
         * use plain image view included in the library.
         *
         * @param tileLayout Tile layout resource ID
         * @return This builder instance
         *//*
        public Builder setTileLayout(@LayoutRes int tileLayout) {
            this.tileLayout = tileLayout;
            return this;
        }

        *//**
         * Sets a drawable for the picker option tile. Default is to use the material design
         * version included in the library.
         *
         * @param pickerDrawable Picker drawable instance
         * @return This builder instance
         *//*
        public Builder setPickerDrawable(Drawable pickerDrawable) {
            this.pickerDrawable = pickerDrawable;
            return this;
        }

        @CheckResult
        public ImagePickerSheetView create() {
            if (imageProvider == null) {
                throw new IllegalStateException("Must provide an ImageProvider!");
            }
            return new ImagePickerSheetView(this);
        }*/
    }

}
