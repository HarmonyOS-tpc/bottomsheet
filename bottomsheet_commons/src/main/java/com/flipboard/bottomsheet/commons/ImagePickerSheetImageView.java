package com.flipboard.bottomsheet.commons;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Image;
import ohos.app.Context;

public class ImagePickerSheetImageView extends Image {

    public ImagePickerSheetImageView(Context context) {
        super(context);
    }

    public ImagePickerSheetImageView(Context context, AttrSet attrs) {
        super(context, attrs);
    }

    public ImagePickerSheetImageView(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
