package com.flipboard.bottomsheet.commons;

import ohos.aafwk.ability.fraction.Fraction;

public final class AccessFragmentInternals {

    private AccessFragmentInternals() {
        throw new AssertionError("No instances.");
    }

    public static int getContainerId(Fraction fragment) {
        return fragment.CONTEXT_IGNORE_SECURITY;
    }
}
