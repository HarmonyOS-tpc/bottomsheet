package com.flipboard.bottomsheet.commons;

import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.utils.LayoutAlignment;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class MenuSheetView extends StackLayout {

    private int columnWidthDp = 100;


    public interface Filter {
        boolean include(ActivityInfo info);
    }

    public interface OnIntentPickedListener {
        void onIntentPicked(ActivityInfo activityInfo);
    }

    private class SortAlphabetically implements Comparator<ActivityInfo> {
        @Override
        public int compare(ActivityInfo lhs, ActivityInfo rhs) {
            return 0;
        }
    }

    private class FilterNone implements Filter {
        @Override
        public boolean include(ActivityInfo info) {
            return true;
        }
    }

    /**
     * Represents an item in the picker grid
     */
    public static class ActivityInfo {
        public Element icon;
        public Object tag;

        public ActivityInfo(Element icon, String label, Context context, Class<?> clazz) {

        }


    }

    protected final Intent intent;
    protected final Context context;
    protected final TableLayout appGrid;
    protected final Text titleView;
    protected final List<IntentPickerSheetView.ActivityInfo> mixins = new ArrayList<>();
    //protected Adapter adapter;
    protected Filter filter = (Filter) new FilterNone();
    protected Comparator<ActivityInfo> sortMethod = new SortAlphabetically();

    public MenuSheetView(Context context, Intent intent, int titleRes, OnIntentPickedListener listener) {
        this(context, intent, "Intent Picker", listener);
    }

    public MenuSheetView(Context context, final Intent intent, final String title, final OnIntentPickedListener listener) {
        super(context);
        this.intent = intent;
        this.context = context;

        ComponentContainer rootLayout = (ComponentContainer) LayoutScatter.getInstance(context)
                .parse(ResourceTable.Layout_grid_sheet_view, this, true);

        appGrid =(TableLayout) rootLayout.findComponentById(ResourceTable.Id_grid);
        titleView = (Text) rootLayout.findComponentById(ResourceTable.Id_text);
        titleView.setText(title);

        ComponentContainer.LayoutConfig layoutConfig = new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT,
                ComponentContainer.LayoutConfig.MATCH_CONTENT);
        appGrid.setColumnCount(3);
        appGrid.verifyLayoutConfig(layoutConfig);

        setLayoutRefreshedListener(new LayoutRefreshedListener() {
            @Override
            public void onRefreshed(Component component) {
                prepareGridLayout();
                setLayoutRefreshedListener(null);
            }
        });
    }

    public void setSortMethod(Comparator<ActivityInfo> sortMethod) {
        this.sortMethod = sortMethod;
    }

    public void setFilter(Filter filter) {
        this.filter = filter;
    }

    public void setColumnWidthDp(int columnWidthDp) {
        this.columnWidthDp = columnWidthDp;
    }

    private void prepareGridLayout() {
        int itemWidth = appGrid.getWidth() / 3;
        ComponentContainer.LayoutConfig itemLayoutConfig = new ComponentContainer.LayoutConfig(
                itemWidth,
                ComponentContainer.LayoutConfig.MATCH_CONTENT);
        itemLayoutConfig.setMarginTop(20);
        for (int position = 0; position < 16; position++) {
            DirectionalLayout directionalLayout = new DirectionalLayout(context);
            directionalLayout.setOrientation(Component.VERTICAL);
            directionalLayout.setAlignment(LayoutAlignment.CENTER);
            Image image = new Image(context);
            image.setPixelMap(ResourceTable.Media_icont);
            Text text = new Text(context);
            text.setText("Label");
            text.setTextSize(16, Text.TextSizeType.FP);
            directionalLayout.addComponent(image);
            directionalLayout.addComponent(text);
            directionalLayout.setLayoutConfig(itemLayoutConfig);
            appGrid.addComponent(directionalLayout);
        }
    }

}
